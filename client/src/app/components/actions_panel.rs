use super::App;
use common::Action;
use egui::{CentralPanel, Color32, Context, RichText, ScrollArea, TextStyle, Ui};

impl App {
    pub fn draw_actions_panel(&mut self, ctx: &Context) {
        CentralPanel::default().show(ctx, |ui| {
            // TODO: Add title?
            // TODO: Add legend row

            ui.separator();

            let actions = self.cache.get_actions_list();

            match actions.len() {
                0 => empty_list(ui),
                _ => actions_list(ui, actions.as_ref()),
            }
        });
    }
}

fn actions_list(ui: &mut Ui, actions_list: &Vec<Action>) {
    let row_height = ui.text_style_height(&TextStyle::Body);
    let total_rows = actions_list.len();

    ScrollArea::vertical()
        .stick_to_bottom(true)
        .max_height(300.0)
        .show_rows(ui, row_height * 2.0, total_rows, |ui, error_range| {
            for i in error_range {
                row(ui, actions_list.get(i).expect("Index of range"));
            }
        });
}

fn empty_list(ui: &mut Ui) {
    ui.heading("Nothing to see here!");
    ui.label(
        RichText::new("Imagine there's no errors...")
            .italics()
            .color(Color32::DARK_GRAY),
    );
}

fn row(ui: &mut Ui, a: &Action) {
    ui.horizontal(|ui| {
        ui.label(RichText::new(a.r#type.to_string()).strong());
        ui.label(
            a.application
                .name
                .clone()
                .expect("Unnamed Applications currently not implemented"),
        ); // TODO: handle unset names (should be handled by application type tbh)
        ui.label(RichText::new(format!("{} |", a.scheduled.utc)).color(Color32::GRAY));
        ui.label(status_icon(a));
    });
    ui.separator();
}

fn status_icon(a: &Action) -> String {
    match a.completed {
        Some(_) => "✔".to_string(),
        None => "⏳".to_string(),
    }
}
