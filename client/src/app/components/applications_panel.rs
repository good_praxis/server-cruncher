use super::App;
use common::Application;
use egui::{Context, RichText, ScrollArea, Sense, SidePanel, Ui};

impl App {
    pub fn draw_applications_panel(&mut self, ctx: &Context) {
        SidePanel::left("applications_panel").show(ctx, |ui| {
            let mut change = None;
            ui.vertical(|ui| {
                let applications = &self.cache.get_applications_list();

                ScrollArea::vertical().show(ui, |ui| {
                    for application in applications {
                        change = if change.is_none() {
                            self.draw_application_panel(application, ui)
                        } else {
                            change
                        };
                    }
                });
            });
            if let Some(selection) = change {
                self.app_selection = Some(selection);
            }
        });
    }

    // To avoid ownership issues we return on Option<usize> of the potentially clicked-on application
    fn draw_application_panel(&self, application: &Application, ui: &mut Ui) -> Option<usize> {
        let Application {
            id, name, status, ..
        } = application;

        // To make the entire section clickable, we wrap it in a group and call `interact` on the response
        if ui
            .group(|ui| {
                if self.app_selection == Some(*id) {
                    ui.label(
                        RichText::new(name.clone().unwrap_or(String::from("Unnamed")))
                            .heading()
                            .strong()
                            .italics(),
                    );
                } else {
                    ui.heading(name.clone().unwrap_or(String::from("Unnamed")));
                }
                ui.separator();
                ui.label(RichText::new(status.clone().unwrap_or(String::from("Error"))).strong());

                draw_server_subsection(application, ui);
                draw_images_subsection(application, ui);
            })
            .response
            .on_hover_cursor(egui::CursorIcon::PointingHand)
            .interact(Sense::click())
            .clicked()
        {
            // TODO: potentially switch to Rc for selection?
            return Some(id.clone());
        }
        None
    }
}

fn draw_server_subsection(application: &Application, ui: &mut Ui) {
    if let Some(servers) = &application.servers {
        ui.label(RichText::new(format!("Servers: {}", servers.len())));
        for server in servers {
            ui.label(format!(
                "IP: {}",
                server.public_net.ipv4.as_ref().unwrap().ip.as_str()
            ));
            ui.add_space(5f32)
        }
    }
}

fn draw_images_subsection(application: &Application, ui: &mut Ui) {
    let count = if application.images.is_some() {
        application.images.as_ref().unwrap().len()
    } else {
        0
    };

    ui.label(RichText::new(format!("Images: {}", count)));
}
