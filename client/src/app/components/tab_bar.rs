use super::App;
use egui::{Align, Context, Layout, TopBottomPanel};

impl App {
    pub fn draw_tab_bar(&mut self, ctx: &Context) {
        TopBottomPanel::top("tab_bar").show(ctx, |ui| {
            ui.horizontal(|ui| {
                // Defaults to the Application tab
                self.selectable_tab("Applications", TabSelection::Applications, ui);
                self.selectable_tab("Actions", TabSelection::Actions, ui);
                self.selectable_tab("Billing", TabSelection::Billing, ui);
                // Flushes remaining items to the right
                ui.with_layout(Layout::right_to_left(Align::Min), |ui| {
                    self.logout_tab(ui);
                    self.error_tab(ui);
                });
            });
        });
    }

    // Small helper function for tooltip-less tabs
    fn selectable_tab(&mut self, label: &str, tab: TabSelection, ui: &mut egui::Ui) {
        if ui
            .selectable_label(self.tab_selection == tab, label)
            .clicked()
        {
            self.tab_selection = tab;
        };
        ui.separator();
    }

    // Single-use function, extracted for indentation. Tab as tooltip
    fn logout_tab(&mut self, ui: &mut egui::Ui) {
        if ui
            .selectable_label(self.tab_selection == TabSelection::Logout, "🚪➡")
            .on_hover_text("Logout")
            .clicked()
        {
            self.tab_selection = TabSelection::Logout
        };
        ui.separator();
    }

    // Single-use function, extracted for indentation. Tab is toggleable (by presence of error log) & has tooltip
    fn error_tab(&mut self, ui: &mut egui::Ui) {
        ui.add_enabled_ui(self.cache.get_error_list().len() > 0, |ui| {
            if ui
                .selectable_label(self.tab_selection == TabSelection::Errors, "⚠")
                .on_hover_text("View Errors")
                .on_disabled_hover_text("No Errors")
                .clicked()
            {
                self.tab_selection = TabSelection::Errors;
            };
            ui.separator();
        });
    }
}

#[derive(Debug, PartialEq)]
pub enum TabSelection {
    Applications,
    Actions,
    Billing,
    Errors,
    Logout,
}
