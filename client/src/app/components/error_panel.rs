use crate::app::api::ApiError;

use super::App;
use egui::{Button, CentralPanel, Color32, Context, RichText, ScrollArea, TextStyle, Ui};

impl App {
    pub fn draw_error_panel(&mut self, ctx: &Context) {
        CentralPanel::default().show(ctx, |ui| {
            let errors = self.cache.get_error_list();
            let mut total_rows = errors.len(); // FIXME: Errors aren't propogating?

            if ui
                .add_enabled(total_rows > 0, Button::new("Clear log"))
                .clicked()
            {
                self.cache.clear_error_list();
                total_rows = 0;
            }
            ui.separator();

            match total_rows {
                0 => empty_list(ui),
                _ => self.error_list(ui, total_rows),
            }
        });
    }

    fn error_list(&mut self, ui: &mut Ui, total_rows: usize) {
        let errors = self.cache.get_error_list();
        let row_height = ui.text_style_height(&TextStyle::Body);

        ScrollArea::vertical().stick_to_bottom(true).show_rows(
            ui,
            row_height * 2.0,
            total_rows,
            |ui, error_range| {
                for i in error_range {
                    row(ui, errors.get(i).expect("Index of range"));
                }
            },
        );
    }
}

fn empty_list(ui: &mut Ui) {
    ui.heading("Nothing to see here!");
    ui.label(
        RichText::new("Imagine there's no errors...")
            .italics()
            .color(Color32::DARK_GRAY),
    );
}

fn row(ui: &mut Ui, e: &ApiError) {
    ui.horizontal(|ui| {
        ui.label(RichText::new(format!("{} |", e.ts.utc)).color(Color32::GRAY));
        ui.label(RichText::new(e.error.to_string()).color(Color32::RED));
    });
    ui.separator();
}
