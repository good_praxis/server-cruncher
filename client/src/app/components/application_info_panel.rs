use super::App;
use common::Application;
use egui::{CentralPanel, Context};

impl App {
    pub fn draw_application_info(&mut self, ctx: &Context) {
        CentralPanel::default().show(ctx, |ui| {
            if let Some(id) = self.app_selection {
                let applications = &self.cache.get_applications_list();
                if applications.len() > 0 {
                    let application = applications
                        .iter()
                        .find(|&app| app.id == id)
                        .expect("Application not found!");

                    let Application { name, status, .. } = application;
                    let mut counter = 0;

                    ui.heading(name.clone().unwrap());

                    ui.label(status.clone().unwrap());

                    if let Some(servers) = &application.servers {
                        for server in servers {
                            ui.label(format!(
                                "IP: {}",
                                server.public_net.ipv4.as_ref().unwrap().ip.as_str()
                            ));
                            ui.label(format!(
                                "Datacenter: {}",
                                server.datacenter.description.as_str()
                            ));
                            ui.label(format!("Status: {:?}", server.status));
                            ui.push_id(counter, |ui| {
                                ui.collapsing("details", |ui| ui.label(format!("{:?}", server)));
                            });
                            counter += 1;
                            ui.separator();
                        }
                    }

                    if let Some(images) = &application.images {
                        for image in images {
                            ui.label(image.description.clone());
                            ui.label(format!("type: {:?}", image.r#type));
                            ui.label(format!("Bound to: {:?}", image.bound_to));
                            ui.label(format!("Status: {:?}", image.status));
                            ui.push_id(counter, |ui| {
                                ui.collapsing("details", |ui| ui.label(format!("{:?}", image)));
                            });
                            counter += 1;
                            ui.separator();
                        }
                    }
                }
            } else {
                empty_panel(ui);
            }
        });
    }
}

fn empty_panel(ui: &mut egui::Ui) {
    ui.vertical_centered_justified(|ui| {
        ui.scope(|ui| {
            ui.add_space(ui.available_height() / 2f32);
            ui.heading("Nothing selected!");
            ui.label("Choose an application from the list to get started.");
        })
    });
}
