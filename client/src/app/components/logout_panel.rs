use super::App;
use egui::{CentralPanel, Context};

impl App {
    pub fn draw_logout_panel(&mut self, ctx: &Context) {
        CentralPanel::default().show(ctx, |ui| {
            ui.vertical_centered(|ui| {
                ui.scope(|ui| {
                    ui.add_space(ui.available_height() / 2f32);
                    ui.label("Are you sure you want to log out?");
                    ui.add_space(5f32);
                    if ui.button("Log out").clicked() {
                        self.logout();
                    }
                })
            });
        });
    }
    fn logout(&mut self) {
        todo!();
    }
}
