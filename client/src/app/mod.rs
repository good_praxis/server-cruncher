mod api;
mod components;

use self::api::Cache;
use components::*;
use serde::{Deserialize, Serialize};
use serde_encrypt::{shared_key::SharedKey, AsSharedKey};

pub(crate) type App = ServerCruncherApp;

/// We derive Deserialize/Serialize so we can persist app state on shutdown.
#[derive(Deserialize, Serialize)]
#[serde(default)] // if we add new fields, give them default values when deserializing old state
pub struct ServerCruncherApp {
    address: String,

    #[serde(skip)]
    cache: Cache,

    local_key: SharedKey,

    #[serde(skip)]
    tab_selection: TabSelection,
    #[serde(skip)]
    app_selection: Option<usize>,
}

impl Default for ServerCruncherApp {
    fn default() -> Self {
        Self {
            address: "0.0.0.0:8080".to_string(),
            cache: Cache::new(),
            local_key: AsSharedKey::generate(),
            tab_selection: TabSelection::Applications,
            app_selection: None,
        }
    }
}

impl ServerCruncherApp {
    /// Called once before the first frame.
    pub fn new(cc: &eframe::CreationContext<'_>) -> Self {
        // This is also where you can customized the look at feel of egui using
        // `cc.egui_ctx.set_visuals` and `cc.egui_ctx.set_fonts`.

        // Load previous app state (if any).
        // Note that you must enable the `persistence` feature for this to work.
        if let Some(storage) = cc.storage {
            let loaded_app: Self = eframe::get_value(storage, eframe::APP_KEY).unwrap_or_default();

            /*             // Unencrypt api key
            if let Some(secret) = loaded_app.hcloud_api_secret {
                loaded_app.hcloud_api_secret = Some(secret.decrypt(&loaded_app.local_key));
                loaded_app.endpoint = Rc::new(Hetzner);
            } */
            //TODO: Remove ^
            return loaded_app;
        }

        Default::default()
    }
}

impl eframe::App for ServerCruncherApp {
    /// Called by the frame work to save state before shutdown.
    fn save(&mut self, storage: &mut dyn eframe::Storage) {
        /*         // Encrypt API keys before writing to disk
        if let Some(secret) = self.hcloud_api_secret.clone() {
            self.hcloud_api_secret = Some(secret.encrypt(&self.local_key));
        } */
        //TODO: Remove ^
        eframe::set_value(storage, eframe::APP_KEY, self);
    }

    /// Called each time the UI needs repainting, which may be many times per second.
    /// Put your widgets into a `SidePanel`, `TopPanel`, `CentralPanel`, `Window` or `Area`.
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        #[cfg(not(target_arch = "wasm32"))] // no File->Quit on web pages!

        // Always present
        self.draw_tab_bar(ctx);

        self.cache.handle_update();
        self.draw_status_bar(ctx);

        match self.tab_selection {
            TabSelection::Applications => {
                // When on Applications Tab
                self.draw_applications_panel(ctx);
                self.draw_application_info(ctx);
            }
            TabSelection::Actions => self.draw_actions_panel(ctx), //TODO: Implement
            TabSelection::Billing => (), //TODO: Implement -- non-vital, skip for initial release
            TabSelection::Errors => self.draw_error_panel(ctx),
            TabSelection::Logout => self.draw_logout_panel(ctx),
        }
    }
}
