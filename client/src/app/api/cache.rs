use common::{Action, Application, CachedData, Timestamp, TTL};
use std::{
    collections::HashMap,
    sync::{
        mpsc::{Receiver, Sender},
        Arc,
    },
    time::Duration,
};

pub enum RequestType {
    ApplicationsList,
    ActionsList,
    Application(usize),
}

pub enum ResponseType {
    ApplicationList(Vec<Application>),
    ActionsList(Vec<Action>),
    Application(usize, Application),
    Error(String),
}

#[derive(Debug)]
pub struct ApiError {
    pub error: String,
    pub ts: Timestamp,
}
impl ApiError {
    pub fn new(error: String) -> Self {
        Self {
            error,
            ts: Timestamp::now(),
        }
    }
}

pub struct Cache {
    tx: Sender<ResponseType>,
    rx: Receiver<ResponseType>,
    applications_list: CachedData<Vec<Application>>,
    actions_list: CachedData<Vec<Action>>,
    application_map: HashMap<usize, CachedData<Option<Application>>>,
    api_errors: Vec<ApiError>,
}
impl Cache {
    pub fn new() -> Self {
        // For now, this is where the API refresh functions will be set
        let (tx, rx) = std::sync::mpsc::channel();

        Self {
            tx: tx.clone(),
            rx,
            applications_list: CachedData::new(super::configure_req_actions_list(
                &"http://localhost:8080/actions".to_string(),
                tx.clone(),
            )), // Required or crash
            actions_list: CachedData::new(super::configure_req_actions_list(
                &"http://localhost:8080/actions".to_string(),
                tx.clone(),
            )),
            application_map: Default::default(),
            api_errors: Default::default(),
        }
    }

    pub fn get_applications_list(&mut self) -> Vec<Application> {
        self.request_data(RequestType::ApplicationsList);
        self.applications_list.get_data()
    }
    pub fn get_actions_list(&mut self) -> Vec<Action> {
        self.request_data(RequestType::ActionsList);
        self.actions_list.get_data()
    }
    pub fn get_application(&mut self, id: usize) -> Option<Application> {
        self.request_data(RequestType::Application(id));
        todo!()
    }
    pub fn get_error_list(&self) -> &Vec<ApiError> {
        &self.api_errors
    }
    pub fn clear_error_list(&mut self) {
        self.api_errors.clear();
    }

    pub fn request_data(&mut self, r#type: RequestType) {
        use RequestType::*;
        let tx = self.tx.clone();

        match r#type {
            ApplicationsList => {
                self.applications_list.update_request();
            }
            ActionsList => {
                self.actions_list.update_request();
            }
            Application(id) => {
                if let Some(app) = self.application_map.get_mut(&id) {
                    app.update_request()
                } else {
                    self.application_map
                        .insert(id, CachedData::new(Arc::new(|| todo!()))); // TODO: properly insert update function for single application
                }
            }
        }
    }

    pub fn handle_update(&mut self) {
        use ResponseType::*;

        // Handle incoming data
        if let Ok(incoming) = self.rx.try_recv() {
            match incoming {
                ApplicationList(data) => {
                    self.applications_list = self
                        .applications_list
                        .update_data(data, TTL::new(Duration::from_secs(1)));
                }
                ActionsList(data) => {
                    self.actions_list = self
                        .actions_list
                        .update_data(data, TTL::new(Duration::from_secs(1)));
                }
                Application(id, data) => {
                    let old_data = self.application_map.get_mut(&id);
                    if let Some(app) = old_data {
                        let app = app.update_data(Some(data), TTL::new(Duration::from_secs(1)));
                        self.application_map.insert(id, app);
                    } else {
                        self.application_map.insert(
                            id,
                            CachedData::new_full(
                                Arc::new(|| todo!()),
                                Some(data),
                                TTL::new(Duration::from_secs(1)),
                            ),
                        );
                    }
                }
                Error(e) => {
                    self.api_errors.push(ApiError::new(e));
                }
            }
        }

        // Reset timed-out progress flags
        self.applications_list.progress.reset_expired();
        self.actions_list.progress.reset_expired();
        for app in self.application_map.values_mut() {
            app.progress.reset_expired();
        }
    }
}
