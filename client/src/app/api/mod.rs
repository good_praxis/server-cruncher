use common::ActionsResponse;
use ehttp::{fetch, Request};
use std::{
    collections::BTreeMap,
    sync::{mpsc::Sender, Arc},
};

mod cache;
pub use cache::{ApiError, Cache, RequestType, ResponseType};

pub fn configure_req_actions_list(
    address: &String,
    tx: Sender<ResponseType>,
) -> Arc<dyn Fn() -> ()> {
    let address = address.to_owned();
    Arc::new(move || {
        let req = Request {
            body: Vec::new(),
            headers: BTreeMap::new(),
            method: "GET".to_string(),
            url: address.clone(),
        };
        let tx = tx.clone();

        fetch(req, move |res| {
            match res {
                Err(e) => tx
                    .send(ResponseType::Error(e.to_string()))
                    .expect("Unable to send through channel"),
                Ok(response) => {
                    let response: ActionsResponse =
                        serde_json::from_str(response.text().expect("No request body"))
                            .expect("Error parsing result body");
                    tx.send(ResponseType::ActionsList(response.data))
                        .expect("Unable to send through channel")
                }
            };
        });
    })
}
