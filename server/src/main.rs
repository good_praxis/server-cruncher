use axum::{routing::get, Json, Router};
use common::{dev::actions_seed, ActionsResponse};

async fn get_actions() -> Json<ActionsResponse> {
    Json(ActionsResponse::new(actions_seed()))
}

#[tokio::main]
async fn main() {
    let app = Router::new().route("/actions", get(get_actions));

    axum::Server::bind(&"0.0.0.0:8080".parse().expect("Couldn't parse IP"))
        .serve(app.into_make_service())
        .await
        .expect("Couldn't bind or launch server")
}
