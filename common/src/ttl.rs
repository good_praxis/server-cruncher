use std::time::{Duration, Instant};

#[derive(Debug, Clone)]
pub struct TTL {
    pub ttl: Instant,
}
impl TTL {
    pub fn new(duration: Duration) -> Self {
        Self {
            ttl: Instant::now() + duration,
        }
    }
    pub fn has_expired(&self) -> bool {
        self.ttl < Instant::now()
    }
}
