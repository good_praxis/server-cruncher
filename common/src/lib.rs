use chrono::Utc;
use serde::{Deserialize, Serialize};

mod timestamp;
pub use timestamp::Timestamp;

mod ttl;
pub use ttl::TTL;

mod application;
pub use application::{generate_application_list, Application};

mod secret;
pub use secret::{Key, Secret};

mod action;
pub use action::{Action, ActionsResponse};

mod progress_flag;
pub use progress_flag::ProgressFlag;

mod cached_data;
pub use cached_data::CachedData;

pub mod dev; // TODO: Remove

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct RemoteData {
    pub data: Data,
    pub updated_at: Timestamp,
    pub origin: String,
}

impl RemoteData {
    pub fn new(data: Data, origin: &str) -> Self {
        Self {
            data,
            updated_at: Timestamp::new(Utc::now()),
            origin: origin.to_string(),
        }
    }
}

#[derive(Debug, Clone, Deserialize, Serialize)]
pub enum Data {
    Application(Vec<Application>),
    Actions(Vec<Action>),
    Error(String),
}
impl Data {
    pub fn actions(self) -> Option<Vec<Action>> {
        if let Data::Actions(list) = self {
            Some(list)
        } else {
            None
        }
    }
}

#[derive(Debug, Clone)]
pub struct Error {
    pub error: String,
    pub ts: Timestamp,
}
impl Error {
    pub fn new(e: &str) -> Self {
        Self {
            error: e.to_string(),
            ts: Timestamp::now(),
        }
    }
}

#[cfg(test)]
mod testing;
#[cfg(test)]
pub use testing::{empty_server, empty_snapshot};
