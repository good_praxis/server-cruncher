use crate::{Application, Timestamp};
use core::fmt;

#[derive(Debug, Clone, serde::Deserialize, serde::Serialize)]
pub struct Action {
    pub id: usize,
    pub r#type: ActionType,
    pub application: Application,
    pub scheduled: Timestamp,
    pub completed: Option<Timestamp>,
}

#[derive(Debug, Clone, PartialEq, serde::Deserialize, serde::Serialize)]
pub enum ActionType {
    // tbd, for now mocked types:
    Creation,
    ShutDown,
    Backup,
    Deletion,
}
impl fmt::Display for ActionType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

#[derive(Debug, Clone, serde::Deserialize, serde::Serialize)]
pub struct ActionsResponse {
    pub data: Vec<Action>,
}
impl ActionsResponse {
    pub fn new(from: Vec<Action>) -> Self {
        Self { data: from }
    }
}
