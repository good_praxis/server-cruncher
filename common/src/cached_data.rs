//! Expirable datatype with an accesibile [ProgressFlag], used to help with rate-limiting
use crate::{ProgressFlag, TTL};
use std::sync::Arc;

pub struct CachedData<T>
where
    T: Default,
{
    data: T,
    ttl: Option<TTL>,
    refresh: Arc<dyn Fn()>,
    pub progress: ProgressFlag,
}
impl<T> CachedData<T>
where
    T: Default + Clone,
{
    pub fn new(refresh: Arc<dyn Fn()>) -> Self {
        Self {
            data: Default::default(),
            ttl: None,
            refresh,
            progress: ProgressFlag::default(),
        }
    }
    pub fn new_full(refresh: Arc<dyn Fn() -> ()>, data: T, ttl: TTL) -> Self {
        Self::new(refresh).update_data(data, ttl)
    }
    pub fn is_valid(&self) -> bool {
        // Data is valid if ttl is set and has not expired
        // Furthermore if the refresh is not in progress
        if let Some(ttl) = &self.ttl {
            return !self.progress.in_progress() && !ttl.has_expired();
        }
        false
    }
    pub fn get_data(&self) -> T {
        self.data.clone()
    }
    pub fn update_request(&mut self) {
        if !self.is_valid() && !self.progress.in_progress() {
            self.progress = ProgressFlag::new();
            (self.refresh)()
        }
    }
    pub fn update_data(&mut self, data: T, ttl: TTL) -> Self {
        Self {
            data,
            ttl: Some(ttl),
            refresh: self.refresh.clone(),
            progress: ProgressFlag::default(),
        }
    }
}
