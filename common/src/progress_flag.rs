//! A custom data-type acting as an expirable boolean. Useful for timeouts of remote calls

use crate::TTL;
use std::time::Duration;
pub struct ProgressFlag {
    active: bool,
    ttl: TTL,
}
impl ProgressFlag {
    pub fn new() -> Self {
        Self {
            active: true,
            ttl: TTL::new(Duration::from_secs(10)),
        }
    }
    pub fn default() -> Self {
        Self {
            active: false,
            ttl: TTL::new(Duration::from_secs(0)),
        }
    }
    pub fn in_progress(&self) -> bool {
        self.active
    }
    pub fn reset_expired(&mut self) {
        if self.active && self.ttl.has_expired() {
            self.active = false;
        }
    }
}
