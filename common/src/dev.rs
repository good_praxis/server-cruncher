// All the content herewithin should be removed ahead of a full release, or at the very least
// kept exclusively for testing purposes

use crate::{action::ActionType, Action, Application, Timestamp};

pub fn actions_seed() -> Vec<Action> {
    let application = Application {
        id: 0,
        name: Some("Minecraft Forge".to_string()),
        images: None,
        servers: None,
        status: None,
    };

    vec![
        Action {
            id: 0,
            r#type: ActionType::Creation,
            application: application.clone(),
            scheduled: Timestamp::now(),
            completed: Some(Timestamp::now()),
        },
        Action {
            id: 0,
            r#type: ActionType::ShutDown,
            application: application.clone(),
            scheduled: Timestamp::now(),
            completed: Some(Timestamp::now()),
        },
        Action {
            id: 0,
            r#type: ActionType::Backup,
            application: application.clone(),
            scheduled: Timestamp::now(),
            completed: Some(Timestamp::now()),
        },
        Action {
            id: 0,
            r#type: ActionType::Creation,
            application: application.clone(),
            scheduled: Timestamp::now(),
            completed: None,
        },
        Action {
            id: 0,
            r#type: ActionType::ShutDown,
            application: application.clone(),
            scheduled: Timestamp::now(),
            completed: None,
        },
        Action {
            id: 0,
            r#type: ActionType::Backup,
            application: application.clone(),
            scheduled: Timestamp::now(),
            completed: None,
        },
    ]
}
